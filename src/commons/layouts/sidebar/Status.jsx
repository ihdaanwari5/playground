
export const Status = () => {
  return (
    <span className="text-sm text-neutral-600 dark:text-neutral-300">
      Available for hire
    </span>
  );
};
