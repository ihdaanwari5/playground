export const BreakLine = ({ className = "" }) => {
  return (
    <div className={className}>
      <div className={`border-t border-gray-300 my-4`}></div>
    </div>
  );
};
