export const CareerList = [
  {
    company: "AVPN Asia",
    companyUrl: "https://avpn.asia/",
    location: "Singapura (Remote)",
    position: "Front-End Developer Intern",
    companyImg: "/avpn.png",
    period: "Mei 2022 - Agu 2022",
    time: 3,
  },
  {
    company: "Sekolah.Mu",
    companyUrl: "https://www.sekolah.mu/",
    location: "Jakarta Timur",
    position: "Front-End Developer Intern",
    companyImg: "/sekolahmu.png",
    period: "Sep 2022 - Des 2022",
    time: 3,
  },
  {
    company: "Antares",
    companyUrl: "https://antares.id/",
    location: "Bandung",
    position: "Developer Intern",
    companyImg: "/antares2.png",
    period: "Jan 2023 - Jun 2023",
    time: 6,
  },
];
